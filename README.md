# Symptoms and Diseases iOS Application 

## Overview
The application is designed and produced with the main feature to gather observed symptoms of user to predict various diseases that user can have. User chooses from a long list of medical symptoms with its scientific name, the application will calculate the possibility of such the diseases that consists of the selected symptoms and order the prediction results based on each one’s possibility percentage.

## Idea and main algorithm
For example:
![alt text](illustration.png "Example")

Therefore, from the above example, it can be concluded a list of predicted diseases:
Hepatitis B: 11.1%, Fibroid Tumor: 20%, Candidiasis: 7.14%

## Availability and Testing
If you have a desire to test this application on your iPhone, it is **not recommended** to clone this project and run on your Xcode because it does not consist my Google Firebase property list file which is my proper credential so that the project cannot show and process any data. Otherwise, it is **strongly recommended** to drop me an email to van_hung_nguyen@yahoo.com then I can add you external testers in my TestFlight so that you can test the app on your physical devices.

## Data Source
The application development uses data from UMLS references from https://www.nlm.nih.gov/research/umls/index.html for names and linked symptopms, and UMLS - NCIT matching ontology on NCI Thesaurus OBO Edition (https://www.ebi.ac.uk/ols/ontologies/ncit) to find defintions of symptoms and diseases.
