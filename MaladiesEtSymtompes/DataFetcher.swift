//
//  MovieFetcher.swift
//  AmisDeVin
//
//  Created by Hung Nguyen on 11/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import Combine
import Firebase
import FirebaseCore
import FirebaseFirestore
import FirebaseFirestoreSwift
import CodableFirebase

public class DataFetcher: ObservableObject {
    @Published var email: String = ""
    @Published var loggedIn = false
    @Published var diseases = [Disease]()
    @Published var guessedDataFirestore: [DataRecord] = []
    @Published var protoSymptoms = [ProtoSymptom]()
    @Published var symptomsSelections :  [Symptom] = []
    @Published var diseaseInfoInView : DiseaseInfoNCIT = DiseaseInfoNCIT()
    
    init(){
        self.diseases = loadCSV("diseases.json")
        self.protoSymptoms = loadCSV("symptoms.json")
        
        if Auth.auth().currentUser != nil {
            if let user = Auth.auth().currentUser {
                updateFromFirestore(email: user.email!)
            }
            
        }
    }
    
    func addToSymptoms(symptom: Symptom)  {
        self.symptomsSelections.append(symptom)
        print(self.symptomsSelections)
    }
    
    func removeToSymptoms(symptom: Symptom)  {
        self.symptomsSelections = self.symptomsSelections.filter { $0 != symptom }
    }
    
    func makeLoggedIn(email: String)  {
        self.loggedIn = true
        self.email = email
        updateFromFirestore(email: email)
        print("Logged in successfully")
    }
    
    func makeLoggedOut(){
        self.email = ""
        self.loggedIn = false
        self.guessedDataFirestore.removeAll()
        self.symptomsSelections.removeAll()
        self.diseaseInfoInView = DiseaseInfoNCIT()
        self.detachListener()
    }
    
    func getDiseaseInfoInView(diseaseCode: String){
        let docRef = db.collection("disease_definitions").document(diseaseCode)
        var toReturn: DiseaseInfoNCIT = DiseaseInfoNCIT()
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                
                DispatchQueue.main.async() {
                    do {
                        toReturn = (try FirestoreDecoder().decode(DiseaseInfoNCIT.self, from: document.data()!))
                        self.diseaseInfoInView = (toReturn)
                    } catch let error {
                        print("Document has error: " + error.localizedDescription)
                    }
                }
                
            } else {
                print("Document does not exist")
            }
        }
    }
    
    
    func loadCSV<T: Decodable>(_ filename: String) -> T {
        let data: Data
        
        guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
            else {
                fatalError("Couldn't find \(filename) in main bundle.")
        }
        
        do {
            data = try Data(contentsOf: file)
        } catch {
            fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
        }
        
        do {
            let decoder = JSONDecoder()
            return try decoder.decode(T.self, from: data)
        } catch {
            fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
        }
    }
    
    func getDataToFirestore(email: String) {
        
        try! db.collection("user_records").whereField("email", isEqualTo: email)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    print(querySnapshot!.documents.count)
                    
                    for document in querySnapshot!.documents {
                        
                        do {
                            let dataRecord = try FirestoreDecoder().decode(DataRecord.self, from: document.data())
                            self.guessedDataFirestore.append(dataRecord)
                            
                        } catch let error {
                            print("Document has error: " + error.localizedDescription)
                        }
                        
                        
                    }
                }
        }
    }
    
    func updateFromFirestore(email: String) {
        
        try! db.collection("user_records").whereField("email", isEqualTo: email)
            .addSnapshotListener { querySnapshot, error in
                
                guard let snapshot = querySnapshot else {
                    print("Error fetching snapshots: \(error!)")
                    return
                }
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added) {
                        
                        do {
                            let dataRecord = try FirestoreDecoder().decode(DataRecord.self, from: diff.document.data())
                            self.guessedDataFirestore.append(dataRecord)
                            self.guessedDataFirestore.sort(by: { $0.stamp > $1.stamp })
                            
                        } catch let error {
                            print("Document has error: " + error.localizedDescription)
                        }
                    }
                    if (diff.type == .modified) {
                        print("Modified: \(diff.document.data())")
                    }
                    if (diff.type == .removed) {
                        print("Removed: \(diff.document.data())")
                    }
                }
                
                if error != nil {
                    print(error!.localizedDescription)
                }
        }
        
    }
    
    private func detachListener() {
        // [START detach_listener]
        let listener = db.collection("user_records").addSnapshotListener { querySnapshot, error in
            // [START_EXCLUDE]
            // [END_EXCLUDE]
        }
        
        // ...
        // Stop listening to changes
        listener.remove()
        // [END detach_listener]
    }
    
}
