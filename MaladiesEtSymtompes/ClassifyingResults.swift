//
//  LoginScreen.swift
//  AmisDeVin
//
//  Created by Hung Nguyen on 13/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import SwiftUI
import Firebase

struct ClassifyingResults:  View {
    @EnvironmentObject var fetcher: DataFetcher
    @State private var showModal = false
    @State private var showingAlert = false
    @State private var saved = false
    
    var guessedList: [GuessedDisease] {
        return  generatePossibility(selectedSymptoms: fetcher.symptomsSelections, allDiseases: fetcher.diseases)
    }
    
    var body: some View {
        //        NavigationView{
        List{
            ForEach(self.guessedList, id: \.self) { disease in
                
                ResultRow(disease: disease)
            }
        }
        .navigationBarTitle("Diagnosis")
        .navigationBarItems(trailing:
            Button(action: {
                
                if self.saved == false {
                    if let sender = Auth.auth().currentUser?.email {
                        addDataToFirestore(guessedDiseases: self.guessedList, email: sender)
                    } else {
                        print("Cannot take the email, use an example email")
                         addDataToFirestore(guessedDiseases: self.guessedList, email: "hung@hung.com")
                    }
                    self.showingAlert = true
                    self.saved = true
                }
                
            }
            ) {
                Text("Save")
            }.alert(isPresented: $showingAlert) {
                Alert(title: Text("Saved"), message: Text("The diagnosis details are saved in your tracking list in your account."), dismissButton: .default(Text("OK")))
            }
            
            
        )
        
    }
    
    
    
    
    func generatePossibility(selectedSymptoms: [Symptom], allDiseases: [Disease]) -> [GuessedDisease] {
        print("generatePossibility() -> [GuessedDisease] {")
        print(selectedSymptoms)
        var preGuessedList: [Disease] = []
        
        for disease in allDiseases {
            for symptom in selectedSymptoms {
                if disease.symptoms.contains(symptom) {
                    preGuessedList.append(disease)
                }
            }
        }
        
        var seen = Set<String>()
        var guessedDiseases: [GuessedDisease] = [   ]
        for disease in preGuessedList {
            if !seen.contains(disease.name) {
                guessedDiseases.append(GuessedDisease(baseDisease: disease, observedSymptoms: []))
                seen.insert(disease.name)
            }
        }
        
        
        for symptom in selectedSymptoms {
            for (index, _) in  guessedDiseases.enumerated() {
                if guessedDiseases[index].baseDisease.symptoms.contains(symptom) {
                    guessedDiseases[index].observedSymptoms.append(symptom)
                }
            }
        }
        
        guessedDiseases.sort(by: { $0.possibility > $1.possibility })
        
        return guessedDiseases
        
    }
}


struct GuessedDisease : Hashable, Codable{
    public var baseDisease : Disease
    public var observedSymptoms: [Symptom]
    
    var possibility : Double {
        let result : Double = Double(self.observedSymptoms.count) / Double(self.baseDisease.symptoms.count)
        //let resultString: String = String(format: "%.1f", result*100)
        return result
    }
    
    enum CodingKeys: String, CodingKey {
           case baseDisease
           case observedSymptoms
       }

}
