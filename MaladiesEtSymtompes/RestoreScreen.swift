import Foundation
import SwiftUI
import Firebase

struct RestoreScreen:  View {
    @EnvironmentObject var fetcher: DataFetcher
    @State private var showModal = false
    
    var body : some View {
        NavigationView {
            List {
                ForEach( self.fetcher.guessedDataFirestore, id : \.self) { eachData in
                    
                    NavigationLink(destination: SymptomsRestoreChoosing(basePrediction: eachData)
                    
                    ){
                        HStack{
                            VStack{
                                Text(getReadableDate(timeStamp: eachData.stamp)[0])
                                Text(getReadableDate(timeStamp: eachData.stamp)[1])
                            }.frame(minWidth: 0, maxWidth: 60, minHeight: 0, maxHeight: 60)
                            
                            VStack(alignment: .leading){
                                Text(eachData.guessedlist[0].disease_name!).fontWeight(.bold)
                                Text("Posibility: " + String(format: "%.1f",  eachData.guessedlist[0].possibility!*100) + "%")
                            }
                            
                            Spacer()
                            Text(getReadableDate(timeStamp: eachData.stamp)[3])
                        }
                    }
                    
                    
                    
                }
            }
            .navigationBarTitle("Testing", displayMode: .automatic)
            .navigationBarItems(trailing:  Button(action: {
                self.fetcher.updateFromFirestore(email: "hung@hung.com")
            }) {
                
                Image(systemName: "arrow.counterclockwise")
                    .foregroundColor(Color.gray)
                
            })
        }
    }
    
    
}

struct SymptomsRestoreChoosing:  View {
    @EnvironmentObject var fetcher: DataFetcher
    @State var selectKeeper = Set<Symptom>()
    @State private var showSelectons = false
    
    public var basePrediction: DataRecord
    private var symptomsList: [Symptom] {
        
        var toReturns : [Symptom] =  []
        
        for eachDisease in basePrediction.guessedlist{
            for eachObservedSymptom in eachDisease.observed_symtomp_names_list! {
                toReturns.append(lookupSymptom(symtompName: eachObservedSymptom))
            }
        }
        
        var seen = Set<String>()
        var unique: [Symptom] = []
        for each in toReturns {
            if !seen.contains(each.symptom_code) {
                unique.append(each)
                seen.insert(each.symptom_code)
            }
        }
        
        return unique
    }
    
    func delete(at offsets: IndexSet) {
           self.fetcher.symptomsSelections.remove(atOffsets: offsets)
       }
    
    func lookupSymptom(symtompName: String ) -> Symptom{
        var toReturns : [Symptom] = []
        for eachDisease in self.fetcher.diseases {
            for eachSymptom in eachDisease.symptoms {
                if symtompName == eachSymptom.symptom_name {
                    toReturns.append(eachSymptom)
                }
            }
        }
        return toReturns[0]
    }
    var body : some View {
        VStack{
           
            List(self.symptomsList, id: \.self, selection: $selectKeeper){ symptom in
                
                     HStack {
                            Text(symptom.symptom_name)
                            Spacer()
                            
                            Button(action: {
                                if self.fetcher.symptomsSelections.contains(symptom) {
                                    self.fetcher.removeToSymptoms(symptom:  symptom)
                                } else {
                                    self.fetcher.addToSymptoms(symptom: symptom)
                                }
                            }) {
                                if self.fetcher.symptomsSelections.contains(symptom) {
                                    Image(systemName: "checkmark.circle.fill")
                                        .foregroundColor(Color.blue)
                                } else {
                                    Image(systemName: "checkmark.circle")
                                        .foregroundColor(Color.gray)
                                }
                            }
                        }
                
                
                     }
            
        }.navigationBarTitle(Text("Selected: \(self.fetcher.symptomsSelections.count)" ))
            .navigationBarItems(trailing:
                HStack {
                    Button(action: {
                        if self.fetcher.symptomsSelections.count != 0 {
                            withAnimation {
                                self.showSelectons.toggle()
                            }}
                    }) {
                        Image(systemName: "tray.full.fill")
                    }
                    Text(String(self.fetcher.symptomsSelections.count))}
                
            )
        .sheet(isPresented: $showSelectons) {
                           
                           VStack(alignment: .leading){
                               Text("You're choosing:")
                               Spacer()
                               Button(action: {
                                   if self.fetcher.symptomsSelections.count != 0 {
                                       withAnimation {
                                           self.fetcher.symptomsSelections.removeAll()
                                       }
                                       
                                   }
                               }) {
                                   Text("Remove All")
                               }
                               Spacer()
                               List{
                                   ForEach(self.fetcher.symptomsSelections, id: \.self ){ symptom in
                                       Text(symptom.symptom_name)
                                   }.onDelete(perform: self.delete)
                               }
                           }.padding()
                   }
        
        
             
    }
    
    
}
