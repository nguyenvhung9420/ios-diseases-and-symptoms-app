// The icon of this application is designed by Smashicons from Flaticons.com


import Foundation
import SwiftUI
import UIKit
import Firebase

struct ContentView: View {
    @EnvironmentObject var fetcher: DataFetcher
    @State var progressValue: Float = 1.0
    @State private var searchText : String = ""
    @State private var showSelectons = false
    
    init(){
        UITableView.appearance().separatorColor = .clear
    }
    
    var symptomsList: [Symptom] {
        var seen = Set<String>()
        var listToReturn: [Symptom] = []
        for disease in self.fetcher.diseases {
            for symptom in disease.symptoms {
                if !seen.contains(symptom.symptom_code) {
                    listToReturn.append(symptom)
                    seen.insert(symptom.symptom_code)
                }
            }
        }
        listToReturn =   listToReturn.sorted(by: { $0.symptom_name < $1.symptom_name })
        return listToReturn
    }
    
    
    var diseasesListView : some View {
        NavigationView {
            
            VStack(alignment: .leading){
                
                List {
                    
                    
                    Text("To get started, choose the symptoms that the patient has")
                    SearchBar(text: $searchText, placeholder: "Search symptoms")
                        .listRowInsets(EdgeInsets(
                            
                            top: 0, leading: 6, bottom: 0, trailing: 6
                        ))
                    
                    
                    ForEach(self.symptomsList.filter {
                        self.searchText.isEmpty ? true : $0.symptom_name.lowercased().contains(self.searchText.lowercased())
                    }, id: \.self) { symptom in
                        HStack {
                            Text(symptom.symptom_name)
                            Spacer()
                            
                            Button(action: {
                                if self.fetcher.symptomsSelections.contains(symptom) {
                                    self.fetcher.removeToSymptoms(symptom:  symptom)
                                } else {
                                    self.fetcher.addToSymptoms(symptom: symptom)
                                }
                            }) {
                                if self.fetcher.symptomsSelections.contains(symptom) {
                                    Image(systemName: "checkmark.circle.fill")
                                        .foregroundColor(Color.blue)
                                } else {
                                    Image(systemName: "checkmark.circle")
                                        .foregroundColor(Color.gray)
                                }
                            }
                        }
                    }
                }
            }.animation(.default)
                
            .navigationBarItems(
                leading:  HStack {
                    Button(action: {
                        if self.fetcher.symptomsSelections.count != 0 {
                            withAnimation {
                                self.showSelectons.toggle()
                            }}
                    }) {
                        Image(systemName: "tray.full.fill")
                    }
                    Text(String(self.fetcher.symptomsSelections.count))
                }.animation(.default),
                
                trailing:
                HStack {
                    
                    if self.fetcher.symptomsSelections.count > 0 {
                        NavigationLink(destination: ClassifyingResults())
                        {
                            HStack{
                                Text("Next")
                                Image(systemName: "chevron.right")
                            }
                          
                            
                        }
                        
                    }
                }.animation(.default)
            )
                .navigationBarTitle(Text("Symptoms"), displayMode: .automatic)
                .sheet(isPresented: $showSelectons) {
                    
                    VStack(alignment: .leading){

                        HStack{
                            Button(action: {
                                self.showSelectons.toggle()
                            }){
                                HStack{
                                    Image(systemName: "xmark")
                                     Text("Dismiss")
                                }
                               
                            }

                            Spacer()
                            Button(action: {
                                if self.fetcher.symptomsSelections.count != 0 {
                                    withAnimation {
                                        self.fetcher.symptomsSelections.removeAll()
                                    }

                                }
                            }) {
                                Text("Remove All (\(self.fetcher.symptomsSelections.count))")
                                    .foregroundColor(Color.red)
                            }

                        }.padding()

                        Text("Current selections")
                            .bold()
                            .font(.largeTitle)
                            .padding()

                        Text("Swipe right to delete selection.")
                            .padding()

                        Spacer()

                        List{
                            ForEach(self.fetcher.symptomsSelections, id: \.self ){ symptom in

                                Text(symptom.symptom_name)
                                    .foregroundColor(Color.white)
                                    .padding()
                                    .background(Color.blue)
                                    .cornerRadius(12)
                                    .listRowInsets(EdgeInsets(top: 0, leading: 12, bottom: 12, trailing: 0   ))


                            }.onDelete(perform: self.delete)
                        }
                    }
            }
        }
        
        
        
    }
    
    func delete(at offsets: IndexSet) {
        self.fetcher.symptomsSelections.remove(atOffsets: offsets)
    }
    
    
    var body: some View {
        VStack {
            
            if Auth.auth().currentUser == nil {
                LoginScreen()
            } else {
                if self.fetcher.diseases.count == 0 {
                    Text("Loading")
                } else {
                    
                    TabView {
                        self.diseasesListView
                            .tabItem {
                                Image(systemName: "waveform.path.ecg")
                                Text("Symptoms")
                        }
                        ProfileView()
                            .tabItem {
                                Image(systemName: "person")
                                Text("Yours")
                        }
                        
                    }
                }
                
            }
        }
        
        
        
        
    }
    
}

struct ProgressBar: View {
    @Binding var value: Float
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                Rectangle().frame(width: geometry.size.width , height: geometry.size.height)
                    .opacity(0.3)
                    .foregroundColor(Color(UIColor.systemTeal))
                
                Rectangle().frame(width: min(CGFloat(self.value)*geometry.size.width, geometry.size.width), height: geometry.size.height)
                    .foregroundColor(Color(UIColor.systemBlue))
                    .animation(.linear)
            }.cornerRadius(45.0)
        }
    }
}

struct Disease: Codable, Hashable {
    
    public var code: String
    public var name: String
    public var rawname: String
    public var count: String
    public var symptoms: [Symptom]
    
    enum CodingKeys: String, CodingKey {
        case code = "disease_code"
        case name = "disease"
        case rawname  = "raw_disease_name"
        case count = "count"
        case symptoms = "symtomps"
    }
}



struct Symptom: Codable, Hashable {
    
    public var symptom_rawname: String
    public var symptom_name: String
    public var symptom_code: String
    public var symptom_syn: String
    public var symptom_syn_code: String
    
    enum CodingKeys: String, CodingKey {
        case symptom_rawname = "symtomp_rawname"
        case symptom_name = "symtomp_name"
        case symptom_code = "symtomp_code"
        case symptom_syn = "symtomp_syn"
        case symptom_syn_code = "symtomp_syn_code"
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(DataFetcher())
    }
}

// extension for keyboard to dismiss
extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
