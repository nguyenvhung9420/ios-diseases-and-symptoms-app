//
//  LoginScreen.swift
//  AmisDeVin
//
//  Created by Hung Nguyen on 13/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import SwiftUI
import Firebase

struct RegisterScreen:  View {
    @EnvironmentObject var fetcher: DataFetcher
    @State var password: String = ""
    @State var email: String = ""
    @State var counter: Int = 0
    
     @State private var favoriteColor = 0
    
    var body: some View {
        
        VStack() {
            VStack {
                
                Picker(selection: $favoriteColor, label: Text("What is your favorite color?")) {
                               Text("Red").tag(0)
                               Text("Green").tag(1)
                               Text("Blue").tag(2)
                           }.pickerStyle(SegmentedPickerStyle())
                
                Text("Register").bold().font(.title)
                
                TextField("Email...", text: self.$email)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .cornerRadius(4.0)
                
                SecureField("Password", text: self.$password) {
                    // submit the password
                } .textFieldStyle(RoundedBorderTextFieldStyle())
                    .cornerRadius(4.0)
                
                HStack() {
                    Spacer()
                    
                    //NavigationButton(destination: DashboardView()) {
                    Text("Forgot Password?").font(.system(size: 15))
                    //  }
                    
                }
                
                Button(action: submit) {
                    //Button() {
                    HStack(alignment: .center) {
                        Spacer()
                        Text("Login").foregroundColor(Color.white).bold()
                        Spacer()
                    }
                }.padding().background(Color.green).cornerRadius(4.0)
                
            }.padding()
            
        }.padding()
    }
    
    func submit() {
        print($email)
        print($password)
        
       
        
        
    }
    
}

