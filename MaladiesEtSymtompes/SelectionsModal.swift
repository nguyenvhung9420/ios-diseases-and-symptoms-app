//
//  SelectionsModal.swift
//  MaladiesEtSymtompes
//
//  Created by Hung Nguyen on 19/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit

struct SelectionsModal: View {
     @Binding var showSelectons: Bool
    @EnvironmentObject var fetcher: DataFetcher
    
    var body: some View {
        VStack(alignment: .leading){
            
            HStack{
                Button(action: {
                    self.showSelectons.toggle()
                }){
                    Text("Dismiss")
                }
                
                Spacer()
                Button(action: {
                    if self.fetcher.symptomsSelections.count != 0 {
                        withAnimation {
                            self.fetcher.symptomsSelections.removeAll()
                        }
                        
                    }
                }) {
                    Text("Remove All (\(self.fetcher.symptomsSelections.count))")
                        .foregroundColor(Color.red)
                }
                
            }.padding()
            
            Text("Current selections")
                .bold()
                .font(.largeTitle)
                .padding()
            
            Text("Swipe right to delete selection.")
                .padding()
            
            Spacer()
            
            List{
                ForEach(self.fetcher.symptomsSelections, id: \.self ){ symptom in
                    
                    Text(symptom.symptom_name)
                        .foregroundColor(Color.white)
                        .padding()
                        .background(Color.blue)
                        .cornerRadius(12)
                        .listRowInsets(EdgeInsets(top: 0, leading: 12, bottom: 12, trailing: 0   ))
                    
                    
                }.onDelete(perform: self.delete)
            }
        }
    }
    
    func delete(at offsets: IndexSet) {
        self.fetcher.symptomsSelections.remove(atOffsets: offsets)
    }
}
