//
//  DiagnosisDetails.swift
//  AmisDeVin
//
//  Created by Hung Nguyen on 13/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import ASCollectionView
import SwiftUI
import QGrid

struct DiagnosisDetails: View {
    //@Binding var showModal: Bool
    var shrinkToSize: Bool = false
    var guessedDisease: GuessedDisease
    private  var symptoms: [Symptom] {
        return guessedDisease.observedSymptoms
    }
    
    private var baseSymptoms: [Symptom] {
        var finalList : [Symptom] = []
        for each in self.guessedDisease.observedSymptoms {
            finalList = self.guessedDisease.baseDisease.symptoms.filter { $0.symptom_code != each.symptom_code }
        }
        return finalList
    }
    
    var body: some View {
        
        VStack{
            VStack(alignment: .leading, spacing: 5){
                Text(self.guessedDisease.baseDisease.name).bold()
                
                HStack(alignment: .center, spacing: 0){
                    GeometryReader { metrics in
                        Rectangle()
                            .fill(getColorOnPercentage(percentage: self.guessedDisease.possibility*100))
                            .frame(
                                width: (metrics.size.width - 20),
                                height: 30,
                                alignment: .topLeading
                        ).opacity(0.25).cornerRadius(5)
                        
                        Rectangle()
                            .fill(getColorOnPercentage(percentage: self.guessedDisease.possibility*100))
                            .frame(
                                width: (metrics.size.width - 20) * (CGFloat(self.guessedDisease.possibility)),
                                height: 30,
                                alignment: .topLeading
                        ).cornerRadius(5)
                        Spacer()
                    }
                    Text(String(format: "%.1f", self.guessedDisease.possibility*100) + "%").bold()
                    .foregroundColor(getColorOnPercentage(percentage: self.guessedDisease.possibility*100))
                }
                    //.background(Color.green) // for debug purpose
                    .frame(height: 30)
            }.padding()
            
            Text("You have \(self.symptoms.count) over \(self.guessedDisease.baseDisease.symptoms.count) symptoms of this disease.")
            
            
            ASCollectionView(data: self.guessedDisease.baseDisease.symptoms.sorted(by: {$0.symptom_name < $1.symptom_name}), dataID: \.self) { item, _ in
                
                NavigationLink(destination: SymptomInfo(symptom_code: item.symptom_code, title: item.symptom_name)){
                    Text(item.symptom_name)
                    .foregroundColor(Color.white)
                    .fixedSize(horizontal: false, vertical: true)
                    .padding(5)
                    .background(self.symptoms.contains(item) ?  Color(.systemBlue) :  Color(.systemGray).opacity(0.5))
                    .cornerRadius(5)
                }
                
            }
            .layout
                {
                    let fl = AlignedFlowLayout()
                    fl.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
                    return fl
            }
            .shrinkToContentSize(isEnabled: shrinkToSize, dimension: .vertical)
            .padding()
        }
            
            
            
            
            //        List{
            //            HStack{
            //                VStack{
            //                    Text("Possibility")
            //                    HStack(){
            //                        Text(String(format: "%.1f", self.guessedDisease.possibility*100))
            //                            .font(.system(size: 55))
            //                        Text("%")
            //                    }
            //                } .padding(.all, 8)
            //                    .foregroundColor(Color.blue)
            //                    .padding()
            //                    .overlay(
            //                        RoundedRectangle(cornerRadius: 11)
            //                            .stroke(Color.blue, lineWidth: 4)
            //                )
            //                    .cornerRadius(10)
            //
            //                VStack{
            //                    Text("Fraction")
            //                    HStack() {
            //                        Text(String(self.symptoms.count)).font(.system(size: 55))
            //                        Text("/")
            //                        Text(String(self.guessedDisease.baseDisease.symptoms.count))
            //                    }
            //                } .padding(.all, 8)
            //                    .foregroundColor(Color.blue)
            //                    .padding()
            //                    .overlay(
            //                        RoundedRectangle(cornerRadius: 11)
            //                            .stroke(Color.blue, lineWidth: 4)
            //                )
            //                    .cornerRadius(10)
            //            }
            //
            //            HStack{
            //                Text("You have")
            //                Text("\(self.symptoms.count)")
            //                Text("over \(self.guessedDisease.baseDisease.symptoms.count) symptoms of this disease.")
            //            }
            //
            //
            //            ASCollectionView(data: self.guessedDisease.baseDisease.symptoms, dataID: \.self) { item, _ in
            //                Color.blue
            //                    .overlay(Text("\(item.symptom_name)"))
            //            }
            //            .layout {
            //                .grid(layoutMode: .adaptive(withMinItemSize: 100),
            //                      itemSpacing: 5,
            //                      lineSpacing: 5,
            //                      itemSize: .absolute(50))
            //            }
            //
            //            //
            //            //            ForEach(self.symptoms, id: \.self){ symptom in
            //            //                Text(symptom.symptom_name)
            //            //                    .fontWeight(.bold)
            //            //                    .frame(height: 32)
            //            //                    .background(Color.blue)
            //            //            }
            //
            ////            GeometryReader { geometry in
            ////                self.generateContent(in: geometry)
            ////            }.frame( height: 400)
            //
            //
            //            //            ForEach(self.baseSymptoms, id: \.self){ symptom in
            //            //                Text(symptom.symptom_name)
            //            //                    .frame(height: 32)
            //            //                    .background(Color.gray)
            //            //
            //            //
            //            //            }
            //
            //        }
            .navigationBarItems(trailing:
                NavigationLink(destination: DiseaseInfo(disease: self.guessedDisease.baseDisease))
                {Image(systemName: "book")})
            .navigationBarTitle(Text(self.guessedDisease.baseDisease.name),
                                
                                displayMode: .inline)
        
        
    }
    
    private func generateContent(in g: GeometryProxy) -> some View {
        var width = CGFloat.zero
        var height = CGFloat.zero
        
        return ZStack(alignment: .topLeading) {
            ForEach(self.guessedDisease.baseDisease.symptoms, id: \.self) { symptom in
                self.item(for: symptom)
                    .padding([.horizontal, .vertical], 4)
                    .alignmentGuide(.leading, computeValue: { d in
                        if (abs(width - d.width) > g.size.width)
                        {
                            width = 0
                            height -= d.height
                        }
                        let result = width
                        if symptom == self.symptoms.last! {
                            width = 0 //last item
                        } else {
                            width -= d.width
                        }
                        return result
                    })
                    .alignmentGuide(.top, computeValue: {d in
                        let result = height
                        if symptom == self.symptoms.last! {
                            height = 0 // last item
                        }
                        return result
                    })
            }
        }
    }
    
    func item(for symptom: Symptom) -> some View {
        
        var color: Color
        
        if self.symptoms.contains(symptom) {
            color = Color.blue
        } else {
            color = Color.gray
        }
        
        return Text(symptom.symptom_name)
            .padding(.all, 8)
            .font(.body)
            .background(color)
            .foregroundColor(Color.white)
            .cornerRadius(5)
        
        
        
    }
}

class AlignedFlowLayout: UICollectionViewFlowLayout
{
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool
    {
        true
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]?
    {
        let attributes = super.layoutAttributesForElements(in: rect)
        
        attributes?.forEach
            { layoutAttribute in
                guard layoutAttribute.representedElementCategory == .cell else
                {
                    return
                }
                layoutAttributesForItem(at: layoutAttribute.indexPath).map { layoutAttribute.frame = $0.frame }
        }
        
        return attributes
    }
    
    private var leftEdge: CGFloat
    {
        guard let insets = collectionView?.adjustedContentInset else
        {
            return sectionInset.left
        }
        return insets.left + sectionInset.left
    }
    
    private var contentWidth: CGFloat?
    {
        guard let collectionViewWidth = collectionView?.frame.size.width,
            let insets = collectionView?.adjustedContentInset else
        {
            return nil
        }
        return collectionViewWidth - insets.left - insets.right - sectionInset.left - sectionInset.right
    }
    
    fileprivate func isFrame(for firstItemAttributes: UICollectionViewLayoutAttributes, inSameLineAsFrameFor secondItemAttributes: UICollectionViewLayoutAttributes) -> Bool
    {
        guard let lineWidth = contentWidth else
        {
            return false
        }
        let firstItemFrame = firstItemAttributes.frame
        let lineFrame = CGRect(
            x: leftEdge,
            y: firstItemFrame.origin.y,
            width: lineWidth,
            height: firstItemFrame.size.height)
        return lineFrame.intersects(secondItemAttributes.frame)
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes?
    {
        guard let attributes = super.layoutAttributesForItem(at: indexPath)?.copy() as? UICollectionViewLayoutAttributes else
        {
            return nil
        }
        guard attributes.representedElementCategory == .cell else
        {
            return attributes
        }
        guard
            indexPath.item > 0,
            let previousAttributes = layoutAttributesForItem(at: IndexPath(item: indexPath.item - 1, section: indexPath.section))
            else
        {
            attributes.frame.origin.x = leftEdge // first item of the section should always be left aligned
            return attributes
        }
        
        if isFrame(for: attributes, inSameLineAsFrameFor: previousAttributes)
        {
            attributes.frame.origin.x = previousAttributes.frame.maxX + minimumInteritemSpacing
        }
        else
        {
            attributes.frame.origin.x = leftEdge
        }
        
        return attributes
    }
}
