//
//  ResultRow.swift
//  AmisDeVin
//
//  Created by Hung Nguyen on 13/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import SwiftUI

struct ResultRow: View {
    @State private var showDetail = false
    //    @State private var showModal = false
    public var disease: GuessedDisease
    
    
    var body: some View {
        
        NavigationLink(destination:
            
            DiagnosisDetails(
                guessedDisease: self.disease
            )
            )
        {
            VStack(alignment: .leading, spacing: 5){
                Text(disease.baseDisease.name)
                HStack(spacing: 0){
                    GeometryReader { metrics in
                        Rectangle()
                            .fill(getColorOnPercentage(percentage: self.disease.possibility*100))
                            .frame(
                                width: metrics.size.width * (CGFloat(self.disease.possibility)), alignment: .topLeading
                        ).cornerRadius(2)
                        Spacer()
                    }
                    Text(String(format: "%.1f", self.disease.possibility*100) + "%")
                }
            }
        }
        
        
        
        //        .sheet(isPresented: $showModal) {
        //            DiagnosisDetails(showModal: self.$showModal,
        //                             guessedDisease: self.disease
        //                             )
        //        }
        
    }
    
}




