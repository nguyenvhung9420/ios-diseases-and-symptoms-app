//
//  SymptomInfo.swift
//  MaladiesEtSymtompes
//
//  Created by Hung Nguyen on 23/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit
import Firebase
import FirebaseCore
import FirebaseFirestore
import FirebaseFirestoreSwift
import CodableFirebase

struct ProtoSymptom: Codable, Hashable {
    public var symptom_code: String = ""
    public var url: String = ""
    var full_url : String {
        return "https://www.ebi.ac.uk/ols/api/ontologies/ncit/terms?iri=\(self.url)"
    }
    
    enum CodingKeys: String, CodingKey {
        case symptom_code = "symptom_code"
        case url = "url"
    }
}

struct SymptomInfo: View {
    public var symptom_code: String
    public var title: String 
    
    @EnvironmentObject var fetcher: DataFetcher
    
    @State private var fetched: Bool?
    @State private var symptomInfoInView: SymptomInfoNCIT = SymptomInfoNCIT()
    
    private var protoSymptomInView : ProtoSymptom {
        var toReturn: ProtoSymptom = ProtoSymptom()
        toReturn = self.fetcher.protoSymptoms.first(where: {$0.symptom_code == self.symptom_code})!
        return toReturn
    }
    
    private func getSymptomInfoInView(symptomURL: String) -> Bool {
        let baseURL: String = "https://www.ebi.ac.uk/ols/api/ontologies/ncit/terms?iri="
        let url = URL(string: (baseURL + symptomURL))!
        
        var urlRequest = URLRequest(url: url)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("application/json", forHTTPHeaderField: "accept")
        urlRequest.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: urlRequest) {(data,response,error) in
            do {
                if let d = data {
                    let decodedData = try JSONDecoder().decode(Level1.self, from: d)
                    //DispatchQueue.main.async {
                    
                    self.symptomInfoInView = decodedData.embedded.terms[0]
                    
                    //}
                } else {
                    print("No Data from ebi.ac.uk")
                }
                
                
            } catch {
                print(error.localizedDescription)
                print ("Error: Cannot get data from ebi.ac.uk")
            }
            
        }.resume()
        
        return true
    }
    
    var body: some View {
        
        if self.protoSymptomInView.url != "na" {
            _ = self.getSymptomInfoInView(symptomURL: self.protoSymptomInView.url)
        }
        
        return VStack{
            
            if self.protoSymptomInView.url == "na" {
                Text("N/A")
            } else {
                List{
                    Text(self.symptomInfoInView.label!)
                        .font(.largeTitle)
                        .bold()
                    
                    SymptomDefinitionElement(
                        section: "Semantic Type", content: self.symptomInfoInView.annotation!.semantic_type!
                    )
                    
                    
                    SymptomDefinitionElement(
                        section: "ULMS CUI", content: self.symptomInfoInView.annotation!.ulms_cui!
                    )
                    
                    SymptomDefinitionElement(
                        section: "NCIT Code", content: self.symptomInfoInView.annotation!.code!
                    )
                    
                    
                    SymptomDefinitionElement(
                        section: "Description", content: self.symptomInfoInView.description!
                    )
                    
                    
                    SymptomDefinitionElement(
                        section: "Alt Definition", content: self.symptomInfoInView.annotation!.alt_definition
                    )
                    
                    
                    
                    
                    
                }
            }
            
            
        }
            
            
            
        .navigationBarTitle(
            Text(self.title)
            
        )
        
        
        
        
    }
    
}

struct SymptomDefinitionElement: View {
    public var section: String
    public var content: [String]?
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5){
            Text(self.section)
                .bold()
                .foregroundColor(Color.white)
                .padding(EdgeInsets(top: 5, leading: 10, bottom: 5, trailing: 10))
                .background(Color.blue)
                .cornerRadius(5.0)
            
            if self.content != nil && content!.count != 0 {
                Text(self.content![0])
            } else {
                Text("N/A")
            }
            
        }.padding(EdgeInsets(top: 4, leading: 0, bottom: 4, trailing: 0))
    }
}

struct Level1 : Codable, Hashable{
    var embedded: Level2
    
    enum CodingKeys : String, CodingKey {
        case embedded = "_embedded"
    }
}

struct Level2 : Codable, Hashable{
    var terms: [SymptomInfoNCIT] = []
    
    enum CodingKeys : String, CodingKey {
        case terms = "terms"
    }
}


struct SymptomInfoNCIT : Codable, Hashable{
    var label: String? = "Loading..."
    var description: [String]? = [""]
    var annotation: SymptomAnnotation? = SymptomAnnotation()
    
    enum CodingKeys : String, CodingKey {
        case label = "label"
        case description = "description"
        case annotation = "annotation"
    }
}

struct SymptomAnnotation: Codable, Hashable {
    var alt_definition: [String]? = ["Loading..."]
    var semantic_type : [String]? = ["Loading..."]
    var ulms_cui : [String]? = ["Loading..."]
    var code : [String]? = ["Loading..."]
    
    enum CodingKeys : String, CodingKey {
        case alt_definition = "ALT_DEFINITION"
        case semantic_type = "Semantic_Type"
        case ulms_cui = "UMLS_CUI"
        case code = "code"
    }
}

