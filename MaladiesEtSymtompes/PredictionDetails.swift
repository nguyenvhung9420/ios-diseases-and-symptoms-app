//
//  PredictionDetails.swift
//  AmisDeVin
//
//  Created by Hung Nguyen on 17/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import SwiftUI



struct PredictionDetails: View {
    @EnvironmentObject var fetcher: DataFetcher
    public var basePrediction: DataRecord
    private var predictions: [GuessedDisease] {
        
        var toReturns : [GuessedDisease] =  []
        
        for eachDisease in basePrediction.guessedlist{
            var newDisease: Disease = lookupDisease(diseaseCode: eachDisease.disease_code!)
            var newObservedSymptoms : [Symptom] = []
            for eachObservedSymptom in eachDisease.observed_symtomp_names_list! {
                newObservedSymptoms.append(lookupSymptom(symtompName: eachObservedSymptom))
            }
            toReturns.append(GuessedDisease(baseDisease: newDisease, observedSymptoms: newObservedSymptoms))
        }
        
        return toReturns
    }
    
    func lookupSymptom(symtompName: String ) -> Symptom{
        var toReturns : [Symptom] = []
        for eachDisease in self.fetcher.diseases {
            for eachSymptom in eachDisease.symptoms {
                if symtompName == eachSymptom.symptom_name {
                    toReturns.append(eachSymptom)
                }
            }
        }
        return toReturns[0]
    }
    
    func lookupDisease(diseaseCode: String ) -> Disease{
        var toReturns : [Disease] = []
        for eachDisease in self.fetcher.diseases {
            if diseaseCode == eachDisease.code {
                toReturns.append(eachDisease)
            }
        }
        return toReturns[0]
    }
    
    var body: some View {
        VStack{
            List {
                NavigationLink(destination: SymptomsRestoreChoosing(basePrediction: basePrediction)){
                    VStack(alignment: .leading, spacing: 8){
                        Text("Restore from this record")
                        HStack{
                            Text(getReadableDate(timeStamp: basePrediction.stamp)[3])
                                .bold()
                                .foregroundColor(Color.white)
                                .padding(EdgeInsets(top: 5, leading: 10, bottom: 5, trailing: 10))
                                .background(Color.blue)
                                .cornerRadius(10.0)
                            
                            Text(
                                getReadableDate(timeStamp: basePrediction.stamp)[0] + " " +
                                    getReadableDate(timeStamp: basePrediction.stamp)[1] + ", " +
                                    getReadableDate(timeStamp: basePrediction.stamp)[2]
                                
                            ).bold()
                                .foregroundColor(Color.white)
                                .padding(EdgeInsets(top: 5, leading: 10, bottom: 5, trailing: 10))
                                .background(Color.blue)
                                .cornerRadius(10.0)
                        }
                    }
                    
                    
                }
                
                ForEach(self.predictions, id: \.self){ prediction in
                    ResultRow(disease: prediction)
                }
            }.listStyle(DefaultListStyle())
        }
        .navigationBarTitle(
            
            
            Text(
                getReadableDate(timeStamp: basePrediction.stamp)[0] + " " +
                    getReadableDate(timeStamp: basePrediction.stamp)[1] + ", " +
                    getReadableDate(timeStamp: basePrediction.stamp)[2]
                
            )
            
            , displayMode: .automatic)
        
    }
}
