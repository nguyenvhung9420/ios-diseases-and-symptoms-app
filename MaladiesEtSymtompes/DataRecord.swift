//
//  DataRecord.swift
//  AmisDeVin
//
//  Created by Hung Nguyen on 15/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import Firebase
import FirebaseCore
import FirebaseFirestore
import FirebaseFirestoreSwift
import CodableFirebase

let db = Firestore.firestore()

extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

struct DataRecord: Hashable, Codable {
    
    var email: String
    var  guessedlist: [FirestoreDiseaseModel]
    
    var stamp: String
    
    enum CodingKeys : String, CodingKey {
        case email = "email"
        case guessedlist = "guessedlist"
        case stamp = "stamp"
    }
}

struct FirestoreDiseaseModel: Hashable, Codable {
    var disease_name: String?
    var disease_code: String?
    var possibility: Double?
    var observed_symtomp_names_list: [String]?
    
    enum CodingKeys : String, CodingKey {
        case disease_name = "disease_name"
        case  disease_code = "disease_code"
        case  observed_symtomp_names_list = "observed_symtomp_names_list"
        case  possibility = "possibility"
    }
}

func addDataToFirestore(guessedDiseases: [GuessedDisease], email: String){
    
    
    var firebaseObjects: [FirestoreDiseaseModel] = []
    
    for guessedDisease in guessedDiseases {
        
        var observedSymtompNamesList: [String ] = []
        
        for symptom in guessedDisease.observedSymptoms {
            observedSymtompNamesList.append(symptom.symptom_name)
        }
        let firebaseObject = FirestoreDiseaseModel(
            disease_name: guessedDisease.baseDisease.name,
            disease_code: guessedDisease.baseDisease.code,
            possibility: guessedDisease.possibility,
            observed_symtomp_names_list: observedSymtompNamesList)
        
        firebaseObjects.append(firebaseObject)
    }
    
    
    let currentTime:String = String(Date().toMillis())
    let stamp: String =  ("\(email)_\(currentTime)")
    let dataRecord = DataRecord(email: email, guessedlist: firebaseObjects, stamp:stamp )
    
    
    //    let docData: [String: Any] = [
    //        "email": email,
    //        "timestamp": Timestamp(date: Date()),
    //        //"guessed":guessedDiseases
    ////        "guessed": [
    ////            "a": 5,
    ////            "b": [
    ////                "nested": "foo"
    ////            ]
    ////        ]
    //    ]
    
    try!  db.collection("user_records").document(dataRecord.stamp).setData(from: dataRecord) { err in
        if let err = err {
            print("Error writing document: \(err)")
        } else {
            print("Document successfully written!")
        }
    }
}

func getDiseaseInfoFirestore(diseaseCode: String){
    let docRef = db.collection("disease_definitions").document("diseaseCode")
    var toReturn: DiseaseInfoNCIT = DiseaseInfoNCIT()
    docRef.getDocument { (document, error) in
        if let document = document, document.exists {
            let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
            print("Document data: \(dataDescription)")
            do {
               toReturn = try FirestoreDecoder().decode(DiseaseInfoNCIT.self, from: document.data()!)
            } catch let error {
                print("Document has error: " + error.localizedDescription)
            }
        } else {
            print("Document does not exist")
        }
    }
}




