//
//  ProfileView.swift
//  AmisDeVin
//
//  Created by Hung Nguyen on 16/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import SwiftUI
import Firebase

func getColorOnPercentage(percentage: Double) -> Color{
    if (percentage >= 11.0) {
        return Color.red
    } else {
        return Color.blue
    }
}

struct JournalRow: View {
    public var eachData : DataRecord
    
    var body: some View {
        HStack {
            
            Rectangle().fill(getColorOnPercentage(percentage: eachData.guessedlist[0].possibility!*100))
                .frame(width: 8.0)
                .cornerRadius(2)
            
            Spacer().frame(width: 20)
            
            
            NavigationLink(destination: PredictionDetails(basePrediction: eachData)){
                HStack{
                    VStack(alignment: .leading){
                        
                        HStack{
                            Text(getReadableDate(timeStamp: eachData.stamp)[0] + " " + getReadableDate(timeStamp: eachData.stamp)[1])
                            Text("|")
                            Text(getReadableDate(timeStamp: eachData.stamp)[3]).fontWeight(.bold)
                        }
                        
                        
                        Text(eachData.guessedlist[0].disease_name!)
                            .fontWeight(.bold)
                        
                        Text("\(eachData.guessedlist.count - 1) other diseases")
                        
                    }
                    
                    Spacer()
                    
                    Text(String(format: "%.1f",  eachData.guessedlist[0].possibility!*100) + "%")
                        .font(.system(size: 20))
                    
                }
            }
        }
    }
}

struct ProfileView:  View {
    @EnvironmentObject var fetcher: DataFetcher
    @State private var showModal = false
    @State private var showSelectons = false
    
    private var stamps: [String] {
        var labels : [String] = []
        for record in self.fetcher.guessedDataFirestore {
            
            let readableStamp = getReadableDate(timeStamp: record.stamp)[0] +  getReadableDate(timeStamp: record.stamp)[1]
            
            if !labels.contains(readableStamp) {
                labels.append(readableStamp)
            }
            
        }
        return labels
    }
    
    
    
    
    var body : some View {
        
        NavigationView {
            List {
                
                ForEach( self.stamps, id : \.self) { stamp in
                    
                    
                    Section(header: Text(stamp)) {
                        
                        ForEach(self.fetcher.guessedDataFirestore.filter {
                            (getReadableDate(timeStamp: $0.stamp)[0] +  getReadableDate(timeStamp:  $0.stamp)[1]) == stamp
                        }, id: \.self) { record in
                            
                            JournalRow(eachData: record)
                            
                        }
                        
                    }
                    
                    
                    
                    
                }
                
            }
            .listStyle(GroupedListStyle())
            .navigationBarTitle("Journal", displayMode: .automatic)
            .navigationBarItems(
                
                
                leading:
                
                //  NavigationLink(destination: ContentView()) {
                Button(action: {
                    let firebaseAuth = Auth.auth()
                    do {
                        try firebaseAuth.signOut()
                        //self.fetcher.loggedIn = false
                        self.fetcher.makeLoggedOut()
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
                }) {
                    HStack{
                        Image(systemName: "power")
                        Text("Log Out")
                    }
                    
                }
                //  }
                ,
                
                trailing:
                HStack {
                    Button(action: {
                        if self.fetcher.symptomsSelections.count != 0 {
                            withAnimation {
                                self.showSelectons.toggle()
                            }}
                    }) {
                        Image(systemName: "tray.full.fill")
                    }
                    Text(String(self.fetcher.symptomsSelections.count))}
                
            )
                .sheet(isPresented: $showSelectons) {
                    
                    
                    VStack(alignment: .leading){
                        HStack{
                            Button(action: {
                                self.showSelectons.toggle()
                            }){
                                Text("Dismiss")
                            }
                            
                            Spacer()
                            Button(action: {
                                if self.fetcher.symptomsSelections.count != 0 {
                                    withAnimation {
                                        self.fetcher.symptomsSelections.removeAll()
                                    }
                                    
                                }
                            }) {
                                Text("Remove All (\(self.fetcher.symptomsSelections.count))")
                                    .foregroundColor(Color.red)
                            }
                            
                        }.padding()
                        
                        Text("Current selections")
                            .bold()
                            .font(.largeTitle)
                            .padding()
                        
                        Text("Swipe right to delete selection.")
                            .padding()
                        
                        Spacer()
                        
                        List{
                            ForEach(self.fetcher.symptomsSelections, id: \.self ){ symptom in
                                
                                Text(symptom.symptom_name)
                                    .foregroundColor(Color.white)
                                    .padding()
                                    .background(Color.blue)
                                    .cornerRadius(12)
                                    .listRowInsets(EdgeInsets(top: 0, leading: 12, bottom: 12, trailing: 0   ))
                                
                                
                            }.onDelete(perform: self.delete)
                        }
                    }
            }
            
            
            
        }
        
        
        
        
    }
    func delete(at offsets: IndexSet) {
        self.fetcher.symptomsSelections.remove(atOffsets: offsets)
    }
    
}



func getReadableDate(timeStamp: String) -> [String] {
    var stamp = timeStamp.split{$0 == "_"}.map(String.init)
    var double = Double(stamp[1])
    double = double!/1000
    
    // let date = Date(timeIntervalSince1970: timeStamp)
    let date = Date(timeIntervalSince1970: double! )
    
    
    let dateFormatterMonth = DateFormatter()
    let dateFormatterDay = DateFormatter()
    let dateFormatterYear = DateFormatter()
    let dateFormatterTime = DateFormatter()
    
    dateFormatterMonth.dateFormat = "MMMM"
    dateFormatterDay.dateFormat = "d"
    dateFormatterYear.dateFormat = "yyyy"
    dateFormatterTime.dateFormat = "HH:mm"
    
    
    return [dateFormatterMonth.string(from: date as Date), dateFormatterDay.string(from: date as Date), dateFormatterYear.string(from: date as Date), dateFormatterTime.string(from: date as Date)]
    
}
