//
//  DiseaseInfo.swift
//  AmisDeVin
//
//  Created by Hung Nguyen on 17/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit
import Firebase
import FirebaseCore
import FirebaseFirestore
import FirebaseFirestoreSwift
import CodableFirebase

struct DiseaseInfo: View {
    @EnvironmentObject var fetcher: DataFetcher
    @State private var fetched: Bool?
    public var disease: Disease?
    private var title: String {
        if self.fetcher.diseaseInfoInView.other_name! != "N/A" {
                   return self.fetcher.diseaseInfoInView.other_name!
               } else {
                   return ("Brief Definition")
               }
    }
    
    init(disease: Disease) {
        self.fetched = false
        self.disease = disease
    }
    
    var body: some View{
        self.fetcher.getDiseaseInfoInView(diseaseCode: self.disease!.code)
       
        return List{
            
            DefinitionElement(section: "Name", content: self.disease!.name)
            
            DefinitionElement(section: "Other Name", content: self.fetcher.diseaseInfoInView.other_name!)
            
            DefinitionElement(section: "ULMS Code", content: self.disease!.code)
            
            DefinitionElement(section: "NCIT Definition", content: self.fetcher.diseaseInfoInView.nci_definition!)
            
        
            
            if self.fetcher.diseaseInfoInView.alt_definition_0! != "N/A"{
                 DefinitionElement(section: "Alt Definition #1", content: self.fetcher.diseaseInfoInView.alt_definition_0!)
            }
            
            if self.fetcher.diseaseInfoInView.alt_definition_1! != "N/A"{
                 DefinitionElement(section: "Alt Definition #1", content: self.fetcher.diseaseInfoInView.alt_definition_1!)
            }
            
            if self.fetcher.diseaseInfoInView.alt_definition_2! != "N/A"{
                 DefinitionElement(section: "Alt Definition #1", content: self.fetcher.diseaseInfoInView.alt_definition_2!)
            }
            
            if self.fetcher.diseaseInfoInView.alt_definition_3! != "N/A"{
                 DefinitionElement(section: "Alt Definition #1", content: self.fetcher.diseaseInfoInView.alt_definition_3!)
            }
            
            
            
            
        }
        .navigationBarTitle(
            Text(self.title)
            
        )
    }
    
}

struct DefinitionElement: View {
    public var section: String
    public var content: String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5){
            Text(self.section)
            .bold()
            .foregroundColor(Color.white)
            .padding(EdgeInsets(top: 5, leading: 10, bottom: 5, trailing: 10))
            .background(Color.blue)
            .cornerRadius(5.0)
            Text(self.content)
        }.padding(EdgeInsets(top: 4, leading: 0, bottom: 4, trailing: 0))
    }
}


struct DiseaseInfoNCIT : Codable, Hashable{
    var disease_code: String? = ""
    var disease_name: String? = ""
    var nci_definition: String? = ""
    var other_code: String? = ""
    var other_name: String? = ""
    var raw_name: String? = ""
    var  definition_provider: String? = ""
    var  alt_definition_0: String? = ""
    var    alt_definition_1: String? = ""
    var   alt_definition_2: String? = ""
    var     alt_definition_3: String? = ""
    
    enum CodingKeys : String, CodingKey {
        case disease_code = "disease_code"
        case disease_name = "disease_name"
        case nci_definition = "nci_definition"
        case other_code = "other_code"
        case other_name = "other_name"
        case raw_name = "raw_name"
        case  definition_provider = "definition_provider"
        case  alt_definition_0 = "alt_definition_0"
        case alt_definition_1 = "alt_definition_1"
        case  alt_definition_2 = "alt_definition_2"
        case  alt_definition_3 = "alt_definition_3"
    }
    
    
    
    
}
