//
//  TestView.swift
//  AmisDeVin
//
//  Created by Hung Nguyen on 18/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import SwiftUI

struct TestView: View {
    @EnvironmentObject var fetcher: DataFetcher
    
    var body: some View {
        NavigationView{
            List(self.fetcher.symptomsSelections , id: \.self){ symptom in
                Text(symptom.symptom_name)
            }
        }
    }
    
    
}

