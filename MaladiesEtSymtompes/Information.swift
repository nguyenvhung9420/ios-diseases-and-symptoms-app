//
//  DiagnosisDetails.swift
//  AmisDeVin
//
//  Created by Hung Nguyen on 13/4/20.
//  Copyright © 2020 Hung Nguyen. All rights reserved.
//

import Foundation
import SwiftUI

struct Information: View {
    @Binding var showModal: Bool
    
    var guessedDisease: GuessedDisease
    private  var symptoms: [Symptom] {
        return guessedDisease.observedSymptoms
    }
    
    var body: some View {
        VStack{
            ForEach(self.symptoms, id: \.self){ symptom in
                VStack{
                    Text(symptom.symptom_name)
                    Text(symptom.symptom_code)
                }
                
            }
            
        }
    }
}
